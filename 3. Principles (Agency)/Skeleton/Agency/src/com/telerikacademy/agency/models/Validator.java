package com.telerikacademy.agency.models;

public class Validator {
    private final static  String PASSANGER_CAPACITY_ERROR_MASSAGE_FOR_TRAIN = "A train cannot have less than 30 passengers or more than 150 passengers.";
    private static final String CARTS_CAPACITY_ERROR_MASSAGE = "A train cannot have less than 1 cart or more than 15 carts.";
    private static final String ARGUMENT_ERROR_MASSAGE = "An argument cannot be null!";
    private static final String PASSANGER_CAPACITY_ERROR_MASSAGE ="A venicle cannot have less than 1 passengers or more than 800 passengers." ;


    public static void setPassangerCapacityForTrain(int passangerCapacity) {
        if (passangerCapacity < 1 || passangerCapacity > 150) {
            throw new IllegalArgumentException(PASSANGER_CAPACITY_ERROR_MASSAGE_FOR_TRAIN);
        }
    }

    public static void setCartsCapacity (int carts) {
        if (carts < 1 || carts > 15) {
            throw new IllegalArgumentException(CARTS_CAPACITY_ERROR_MASSAGE);
        }
    }

    public static void checkForNullArgument (Object o) {
        if (o == null) {
            throw new IllegalArgumentException(ARGUMENT_ERROR_MASSAGE);
        }
    }
    
    public static void setPassangerCapacityForBase(int passangerCapacity) {
        if (passangerCapacity < 1 || passangerCapacity > 800) {
            throw new IllegalArgumentException(PASSANGER_CAPACITY_ERROR_MASSAGE);
        }
        
    }
}
