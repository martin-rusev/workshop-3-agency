package com.telerikacademy.agency.models;

import com.telerikacademy.agency.commands.enums.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    private int carts;

    public TrainImpl(int passangerCapacity, VehicleType type, double pricePerKilometer, int carts) {
        super(passangerCapacity,type, pricePerKilometer);
        setCarts(carts);
    }

    public void setCarts(int carts) {
        Validator.setCartsCapacity(carts);
        this.carts = carts;
    }

    @Override
    public void setPassangerCapacity(int passangerCapacity) {
        if (passangerCapacity < 30 || passangerCapacity > 150) {
            throw new IllegalArgumentException("A train cannot have less than 30 passengers or more than 150 passengers.");
        }
        super.passangerCapacity = passangerCapacity;
    }

    @Override
    public int getCarts() {
        return carts;
    }
}
