package com.telerikacademy.agency.models;

import com.telerikacademy.agency.commands.enums.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {
    private boolean hasFreeFood;

    public AirplaneImpl(int passangerCapacity, VehicleType type, double pricePerKilometer, boolean hasFreeFood) {
        super(passangerCapacity, type, pricePerKilometer);
        this.hasFreeFood = hasFreeFood;
    }

    public boolean isHasFreeFood() {
        return hasFreeFood;
    }

    private void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }
}
