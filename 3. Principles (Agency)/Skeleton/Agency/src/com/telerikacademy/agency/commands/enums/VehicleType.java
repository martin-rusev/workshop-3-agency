package com.telerikacademy.agency.commands.enums;

public enum VehicleType {
    Land,
    Air,
    Sea;
}
