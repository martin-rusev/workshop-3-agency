package com.telerikacademy.agency.models.vehicles.contracts;

import com.telerikacademy.agency.commands.enums.VehicleType;

public interface Vehicle {
    int getPassengerCapacity();

    double getPricePerKilometer();

    VehicleType getType();
}