package com.telerikacademy.agency.models;

import com.telerikacademy.agency.commands.enums.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleBase implements Vehicle {

    private static final String PASSANGER_CAPACITY_ERROR_MASSAGE ="A venicle cannot have less than 1 passengers or more than 800 passengers." ;

    package int passangerCapacity;
    VehicleType type;
    double pricePerKilometer;

    public VehicleBase(int passangerCapacity, VehicleType type, double pricePerKilometer) {
        setPassangerCapacity(passangerCapacity);
        setType(type);
        setPricePerKilometer(pricePerKilometer);
    }

    public int getPassangerCapacity() {
        return passangerCapacity;
    }

    public void setPassangerCapacity(int passangerCapacity) {
        if (passangerCapacity < 1 || passangerCapacity > 800) {
            throw new IllegalArgumentException(PASSANGER_CAPACITY_ERROR_MASSAGE);
        }
        this.passangerCapacity = passangerCapacity;
    }

    private void setType(VehicleType type) {
        Validator.checkForNullArgument(type);
        this.type = type;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public int getPassengerCapacity() {
        return passangerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }
}
